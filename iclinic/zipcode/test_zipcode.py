# -*- coding: utf-8 -*-
import pytest


@pytest.mark.django_db
def test_get_detail_zipcode(client):
    client.post('/zipcode/', 'zip_code=14020260',
                'application/octet-stream')
    response = client.get('/zipcode/14020260/')
    assert response.status_code == 200


@pytest.mark.django_db
def test_list_zipcodes(client):
    response = client.get('/zipcode/')
    assert response.status_code == 200


@pytest.mark.django_db
def test_list_zipcodes_with_limit(client):
    response = client.get('/zipcode/?limit=3')
    assert response.status_code == 200


@pytest.mark.django_db
def test_create_from_zipcode(client):
    response = client.post('/zipcode/', 'zip_code=14020260',
                           'application/octet-stream')
    assert response.status_code == 201


@pytest.mark.django_db
def test_create_from_zipcode_with_invalid_zipcode(client):
    response = client.post('/zipcode/', 'zip_code=1402260',
                           'application/octet-stream')
    assert response.status_code == 400


@pytest.mark.django_db
def test_create_from_zipcode_with_inexistent_zipcode(client):
    response = client.post('/zipcode/', 'zip_code=00000000',
                           'application/octet-stream')
    assert response.status_code == 400


@pytest.mark.django_db
def test_delete_zipcode(client):
    client.post('/zipcode/', 'zip_code=14020260',
                'application/octet-stream')
    response = client.delete('/zipcode/14020260/')
    assert response.status_code == 204


@pytest.mark.django_db
def test_delete_zipcode_doesnt_exist(client):
    response = client.delete('/zipcode/14020260/')
    assert response.status_code == 204
