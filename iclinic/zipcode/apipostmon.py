import json
import requests


def get_cep_data(cep):
    """
    Wrapper that access the postmon API and returns a dict with the data
    """
    url = u'http://api.postmon.com.br/v1/cep/{}'.format(cep)
    response = requests.get(url)
    return json.loads(response.text)
