# -*- coding: utf-8 -*-
from django.db import models


class Zipcode(models.Model):
    """
    Stores zipcodes information
    """
    zip_code = models.CharField('Zip code', max_length=10, unique=True)
    address = models.CharField('Address', max_length=255, null=True, default='')
    neighborhood = models.CharField('Neighborhood', max_length=255, null=True,
                                    default='')
    state = models.CharField('State', max_length=3, null=True, default='')
    city = models.CharField('City', max_length=255, null=True, default='')
