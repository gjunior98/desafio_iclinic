# -*- coding: utf-8 -*-
from restless.dj import DjangoResource
from restless.preparers import FieldsPreparer
from restless.exceptions import BadRequest

from .apipostmon import get_cep_data
from .models import Zipcode
from .serializer import BasicSerializer

import logging

logger = logging.getLogger('zipcode')


class ZipcodeResource(DjangoResource):
    preparer = FieldsPreparer(fields={
        'zip_code': 'zip_code',
        'address': 'address',
        'neighborhood': 'neighborhood',
        'state': 'state',
        'city': 'city',
    })

    serializer = BasicSerializer()

    def handle(self, endpoint, *args, **kwargs):
        """
        Overriding handle method to log all requests to the API
        """
        logger.info(u'method:{} status:{} POST:{} GET:{} kwargs:{}'.format(
            self.request_method(), self.status, str(self.request.POST),
            str(self.request.GET), str(kwargs)))
        return super().handle(endpoint, *args, **kwargs)

    def is_authenticated(self):
        """Only for testing purpose"""
        return True

    def list(self):
        """
        Lists CEPs, accepts GET parameter 'limit'
        """
        limit = int(self.request.GET.get('limit', 0))
        qs = Zipcode.objects.all()
        if limit:
            return qs[:limit]

        return qs

    def detail(self, zip_code):
        """
        Return details of an specific CEP
        """
        try:
            return Zipcode.objects.get(zip_code=zip_code)
        except Zipcode.DoesNotExist:
            pass

    def create(self):
        """
        Access postmon api and records/updates CEP data into the database
        """
        data = {}

        if len(self.data['zip_code']) < 8 or len(self.data['zip_code']) > 8:
            raise BadRequest(u"CEP inválido")

        try:
            zip_data = get_cep_data(self.data['zip_code'])
            data['zip_code'] = self.data['zip_code']
        except:
            raise BadRequest(u'Não foi possível encontrar dados para esse CEP')

        data.update({
            'address': zip_data['logradouro'],
            'neighborhood': zip_data['bairro'],
            'state': zip_data['estado'],
            'city': zip_data['cidade'],
        })

        obj, _ = Zipcode.objects.update_or_create(**data)
        return obj

    def delete(self, zip_code):
        """
        Deletes a CEP
        """
        try:
            Zipcode.objects.get(zip_code=zip_code).delete()
        except Zipcode.DoesNotExist:
            pass
