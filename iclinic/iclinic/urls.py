# -*- coding: utf-8 -*-
from django.conf.urls import url, include

from zipcode.api import ZipcodeResource

urlpatterns = [
    url(r'zipcode/(?P<zip_code>\d+)/$', ZipcodeResource.as_detail(),
        name='api_zipcode_detail'),
    url(r'zipcode/', ZipcodeResource.as_list(), name='api_zipcode_list'),
    url(r'zipcode/', ZipcodeResource.as_detail(), name='api_zipcode_create'),
]
